package com.pnc.utilites;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PncUtilitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PncUtilitiesApplication.class, args);
	}

}
